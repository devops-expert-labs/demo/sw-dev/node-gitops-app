FROM node:18 AS builder
WORKDIR /build
COPY package*.json ./
RUN npm ci --omit=dev
COPY . ./

FROM gcr.io/distroless/nodejs:18
WORKDIR /app
COPY --from=builder /build ./
USER nonroot:nonroot
EXPOSE 8080
CMD ["app.js"]
